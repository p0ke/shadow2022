<?php
echo "";
?>
<div id="main-wrapper">
	<div class="container">
		<div id="content">
			<!-- Content -->
			<article>
	               <section class="widget thumbnails">
	                        <h3>Callisto / flush invitro for Shadow Party 2022 presented at Tokyo Demo Fest</h3>
	                        <div class="grid">
	                                <div class="row gtr-50">
	                                        <div class="col-6"><a href="https://youtu.be/5vQu9QqXNfU" target="_blank" class="image fit"><img src="images/invit_vid.png" alt="" /></a></div>
	                                </div>
	                        </div>
	                        <h3>Shadow Party 2022 invitation presented at French Revision Satelite 2022 (by Alkama, Aubépine, Callisto, Coyhot, p0ke)</h3>
	                        <div class="grid">
	                                <div class="row gtr-50">
	                                        <div class="col-6"><a href="https://www.youtube.com/watch?v=W6ybnfktkRM" target="_blank" class="image fit"><img src="images/invit_vid2.png" alt="" /></a></div>
	                                </div>

	                        </div>
	                </section>
			</article>
		</div>
	</div>
</div>		
