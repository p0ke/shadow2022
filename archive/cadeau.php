<?php

?>
<div id="main-wrapper">
	<div class="container">
		<div id="content">
The following basic code has been used in the <a href="https://youtu.be/W6ybnfktkRM">invitation</a> and has been used on a real Thomson TO9.<br/><br/>
You can replay the code in <a href="http://dcmoto.free.fr/emulateur/index.html">DCMOTO emulator</a> (don't forget to set TO9 machine in the configuration. <br/><br/>
You just need to copy the code and use the copy from clipboard in the emulator, it'll type it for you !!!  <br/><br/>
You can now replay the wonderful scene between assistan & professor !!!!!<br/><br/>
Enjoy !!! <3 p0ke <br/><br/><br/>
<pre>
10 CLEAR,&H8FFF:CLS:SCREEN 2,0,0:LOCATE 0,1
15 ATTRB 1,1
20 PRINT"SECURITY HAZARD"
25 ATTRB 0,0
30 PRINT"========================================"
40 LOCATE 0,7
50 PRINT "1. RELEASE SHADOW PARTY INVITATION"
60 PRINT:PRINT:PRINT:PRINT
70 PRINT "2. RELEASE THE BEAST"
71 PRINT CHR$(20)
80 A$=INKEY$
90 IF A$="1" THEN GOTO 500
100 IF A$="2" THEN GOTO 378
105 IF A$="3" THEN END
110 GOTO 80
300 REM
310 ATTRB 1,1
320 LOCATE 13,5
330 PRINT"WARNING"
340 LOCATE 4,10
350 PRINT "YOU HAVE RELEASE"
360 ATTRB 1,1
365 LOCATE 12,16
370 PRINT "THE BEAST"
371 COLOR 0,0
375  RETURN
378 CLS
379 GOSUB 300
380 FOR I=1 TO 2
390 COLOR 1,0
391 SCREEN ,,1
395 GOSUB 360
400 REM
405 FOR B=1 TO 500:NEXT B
408 COLOR 0,0
410 GOSUB 360
415 REM
418 FOR B=1 TO 500:NEXT B
421 PLAY"DODO"
440 NEXT I
450 GOTO 10
500 CLS
510 ATTRB 1,1
520 LOCATE 2,5
530 PRINT"UPLOADING":LOCATE 2,9:PRINT"INVITATION"
540 PRINT
541 LOCATE 0,12
542 PRINT CHR$(124);"__________";CHR$(124)
550 FOR I=1 TO 20
560 LOCATE I,12:PRINT CHR$(127)
562 LOCATE 0,15:PRINT I*5;"%"
570 NEXT I
580 GOTO 10
</pre>
</div>
</div>
</div>