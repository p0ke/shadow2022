<?php

?>
<div id="main-wrapper">
	<div class="container">
		<div id="content">

<pre>
INFO-LINES


***** 16/05/2022 *****

Looking at all the requests, we decided to push the deadline to the limit !
The dead line changed from 25th of May to 1st of June, yeah you heard it well !!! 1st of June !!!

***** 13/05/2022 *****

Hello @everyone, following your requests we ended up opening the textmode graphic compo, please check the rules here => https://www.shadow-party.org/compo


***** 07/05/2022 *****

<a href="https://vote.shadow-party.org/">https://vote.shadow-party.org/</a> is up and running, 

<u>To register you need a votekey, request one to admins @MooZ @Zavie @Callisto or @p0ke on <a href="https://discord.gg/2mEc4pXvWb" target="_blank">Discord</a>. </u>


***** 04/05/2022 *****

The Shadow Party ( <a href="https://www.shadow-party.org/">https://www.shadow-party.org/</a> ) is approaching: it's next month, the weekend of 10th to 12th of June.
Although we're planning everything for an online event, we're crossing fingers to secure a party place for 50 people by then.

Deadlines for entries is set to 25th of May so we don't have to pull all-nighters to prepare the stream.
We're aware that's quite early and may come as a surprise.
If you're considering to release something but the announced schedule is too tight, do let us know and we'll consider our options.


Party dates : from 10th to 12th of June 2022

WEBSITE : <a href="https://www.shadow-party.org/">https://www.shadow-party.org/</a> 

<a href="https://www.twitch.tv/shadowstreamed">TWITCH</a>		: 	<a href="https://www.twitch.tv/shadowstreamed">https://www.twitch.tv/shadowstreamed</a>
<a href="https://www.facebook.com/shadowparty.org">FACEBOOK</a>	: 	<a href="https://www.facebook.com/shadowparty.org">https://www.facebook.com/shadowparty.org</a> 
<a href="https://twitter.com/shadowparty5">TWITTER</a>	: 	<a href="https://twitter.com/shadowparty5">https://twitter.com/shadowparty5</a> 
<a href="https://www.instagram.com/_shadow.party_/">INSTAGRAM</a>	: 	<a href="https://www.instagram.com/_shadow.party_/">https://www.instagram.com/_shadow.party_/</a> 
<a href="https://www.youtube.com/channel/UCAMab0DseF-Hvq3kGkAtyfg">YOUTUBE</a>	:	<a href="https://www.youtube.com/channel/UCAMab0DseF-Hvq3kGkAtyfg">https://www.youtube.com/channel/UCAMab0DseF-Hvq3kGkAtyfg</a> 

Entry submissions : 	📅 2022-05-01 📅 

Deadline : 		        ⚠️ 2022-05-25 ⚠️

**(Deadlines for entries is set to 25th of May so we don't have to pull all-nighters to prepare the stream.
We're aware that's quite early and may come as a surprise.
If you're considering to release something but the announced schedule is too tight, do let us know and we'll consider our options.)

We suggest you first ask for your votekey to <A HREF="mailto:admin@shadow-party.org">admin@shadow-party.org</A> or directly on discord, 
then follow this link to register : <a href="https://www.shadow-party.org/whoisin">https://www.shadow-party.org/whoisin</a>

• ⚠️ Please contact us if you think your entry will be a little bit large! 
• compos rules : https://www.shadow-party.org/compo
• email: <A HREF="mailto:info@shadow-party.org">info@shadow-party.org</A>

• contacts on <a href="https://discord.gg/2mEc4pXvWb" target="_blank">Discord</a>: 

  - oldschool demo & intro 		: @MooZ 

  - newschool demo & intro 		: @MooZ 

  - newschool gfx					: @Callisto

  - oldschool gfx					: @MooZ 

  - photo 						: @Callisto 

  - music streamed & tracked 		: @p0ke 

  - wild 							: @Zavie 

  - Motion design 				: @Callisto 

  - fantasy console 				: @Zavie 

  - seminars 						: @JeFfR3y 

***** 10/06/2022 *****
Shadow party is over !!
see you next year :)

</pre>
</div>
</div>
</div>