<?php
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}


function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function clean($string)
{
    $string = preg_replace('#[^a-zA-Z0-9/\^. -]#', '', $string);
    $pattern=array('<br>','<br/>','<BR>','<BR/>');
    //$replace=array('','two','tree');
    //$content = preg_replace($pattern,'', $content);
    
    return($string);
}

function findip($ip,$fichier)
{
	$fichref ='';
	if($fichier=='reg')$fichref = 'datafile/registered';
	if($fichier=='mail')$fichref = 'datafile/mailsent';
	if($fichref =='')return("KO");
    foreach(array_count_values(file($fichref)) as $person => $count)
    {
    	$counts[$person] = $count; 
    	if(trim($person)==$ip and $count>=2)return("KO");
    }
    return("OK");
}

function countip($ip,$fichier)
{
	$fichref ='';
	if($fichier=='reg')$fichref = 'datafile/registered';
	if($fichier=='mail')$fichref = 'datafile/mailsent';
	if($fichref =='')return("KO");
	$count=0;
    foreach(array_count_values(file($fichref)) as $person => $count)
    {
    	$counts[$person] = $count; 
    	return($count);
    }
}

function writeip($ip,$fichier)
{
	$fichref ='';
	if($fichier=='reg')$fichref = 'datafile/registered';
	if($fichier=='mail')$fichref = 'datafile/mailsent';
	if($fichref =='')return("KO");
	$message = "$ip\r\n";
	file_put_contents($fichref, $message, FILE_APPEND);
}

function countlines($fichier)
{
	$fichref ='';
	if($fichier=='reg')$fichref = 'datafile/registered';
	if($fichier=='mail')$fichref = 'datafile/mailsent';
	if($fichref =='')return("KO");
	$linecount = 0;
	$handle = fopen($fichref, "r");
	while(!feof($handle)){
  		$line = fgets($handle);
  		$linecount++;
	}
	fclose($handle);
	return($linecount);
}
?>