<?php

?>
<div id="main-wrapper">
	<div class="container">
		<div id="content">

			<!-- Content -->
				<article>
               <h2>Shadow Party Edition 2021</h2>
                  Some figures :<br /><br />

                  &nbsp; &nbsp; 60 compo entries<br />
                  &nbsp; &nbsp; 96 users<br / >
                  &nbsp; &nbsp; 83 votes<br />
                <br/>
                <br/>
               <h3> 2021 Seminar and concert Schedule </h3>
              <pre>
               2021-05-21 18:00:00 	Opening
               2021-05-21 18:45:00 	Photoshop frame by frame animation (Franck Payen)
               2021-05-21 20:05:00 	seminar Build me a space shuttle by p0ke/flush
               2021-05-21 21:00:00 	Shader Jam
               2021-05-21 23:59:00 	Track list & video
               2021-05-22 11:00:00 	Breakfast
               2021-05-22 12:00:00 	Lunch break
               2021-05-22 13:30:00 	seminar From manga to Japan by Sarcan
               2021-05-22 15:00:00 	Break
               2021-05-22 16:00:00 	Break
               2021-05-22 16:30:00 	seminar Build your own demo machine using FPGA by Zerkman
               2021-05-22 17:30:00 	Break
               2021-05-22 19:15:00 	Break
               2021-05-22 20:00:00 	1 hour break
               2021-05-22 21:00:00 	goto80 chilled out mix
               2021-05-22 22:00:00 	Tracklist Video
               2021-05-23 11:00:00 	Breakfast
               2021-05-23 12:00:00 	deadline Voting closed
               2021-05-23 13:00:00 	Closing
            </pre>
            <h3> 2021 Compos Schedule </h3>
            <pre>
               2021-05-21 19:15:00  Old school graphics
               2021-05-21 19:30:00  Photo
               2021-05-21 19:45:00  New School Graphics
               2021-05-22 14:15:00  Music Tracked
               2021-05-22 15:15:00  Music Streamed
               2021-05-22 18:00:00  TIC80 256 Bytes
               2021-05-22 18:15:00  Wild
               2021-05-22 18:30:00  Oldschool intro
               2021-05-22 18:45:00  Oldschool demo
               2021-05-22 19:45:00  New school Intro
               2021-05-22 19:55:00  New school demo
    
            </pre>
               <h3>Released production</h3>
                &nbps; <br />
					<h1>Pouet</h1>
               &nbsp; &nbsp; &nbsp; &nbsp;<a href="https://www.pouet.net/party.php?which=1936" target="_blank">https://www.pouet.net/party.php?which=1936</a>
              <br /><br />
					<h1>Demozoo</h1>
               &nbsp; &nbsp; &nbsp; &nbsp;<a href="https://demozoo.org/parties/4199/" target="_blank">https://demozoo.org/parties/4199/</a>
              <br /><br />
					<h1>Shadow Party old website</h1>
               &nbsp; &nbsp; &nbsp; &nbsp;<a href="http://2021.shadow-party.org/" target="_blank">http://2021.shadow-party.org/</a>
              <br /><br />
				</article>

		</div>
	</div>
</div>		
