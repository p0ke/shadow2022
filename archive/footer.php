<?php 
/*
				
				<div class="col-3 col-6-medium col-12-small">

					<!-- Links -->
						<section class="widget links">
							<h3>Random Stuff</h3>
							<ul class="style2">
								<li><a href="#">Etiam feugiat condimentum</a></li>
								<li><a href="#">Aliquam imperdiet suscipit odio</a></li>
							</ul>
						</section>

				</div>
				

*/
?>
<!-- Footer -->
	<div id="footer-wrapper">
		<footer id="footer" class="container">
			<div class="row">
 				<div class="col-3 col-6-medium col-12-small">

					<!-- Contact -->
						<section class="widget contact last">
							<h3>Contact Us</h3>
							<ul>
								<li><a href="https://twitter.com/ShadowParty5" target="_blank" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="https://www.facebook.com/shadowparty.org" target="_blank"  class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
								<li><a href="https://www.instagram.com/_shadow.party_/" class="icon brands fa-instagram" target="_blank" ><span class="label">Instagram</span></a></li>
								<li><a href="https://www.twitch.tv/shadowstreamed" target="_blank"  class="icon brands fa-twitch"><span class="label">Twitch</span></a></li>
								<li><a href="https://www.youtube.com/channel/UCAMab0DseF-Hvq3kGkAtyfg" target="_blank"  class="icon brands fa-youtube"><span class="label">Youtube</span></a></li>
								<li><a href="https://discord.gg/2mEc4pXvWb" target="_blank" class="icon brands fa-discord"><span class="label">Discord</span></a></li>
							</ul>
						</section>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div id="copyright">
						<ul class="menu">
							<li>&copy; Computer Art Culture Association. All rights reserved (c)2021/2022</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
	</div>

</div>
	<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
		<script src="assets/js/checkval.js"></script>
		<script src="assets/js/lbltoggle.js"></script>
	</body>
</html>