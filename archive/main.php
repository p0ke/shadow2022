<?php

?>
<!-- Main -->
<div id="main-wrapper">
	<div class="container">
		<div class="row gtr-200">
			<div class="col-4 col-12-medium">
				<!-- Sidebar -->
					<div id="sidebar">
						<section class="widget thumbnails">
							<h3>Wanna see some streams from last year ?!?!</h3>
							<div class="grid">
								<div class="row gtr-50">
									<div class="col-6"><a href="https://youtu.be/U4T7HCQekUA?t=343" target="_blank" class="image fit"><img src="images/small_windows_01-mini.png" alt="" /></a></div>
									<div class="col-6"><a href="https://youtu.be/ygr3VH7kYUU?t=7178" target="_blank" class="image fit"><img src="images/small_windows_02-mini.png" alt="" /></a></div>
									<div class="col-6"><a href="https://www.youtube.com/watch?v=I1_k_mdkXXE" target="_blank" class="image fit"><img src="images/tic80.jpg" alt="" /></a></div>
									<div class="col-6"><a href="https://youtu.be/akgC8cbJn9I?t=702" target="_blank" class="image fit"><img src="images/small_windows_03-mini.png" alt="" /></a></div>
									<div class="col-6"><a href="https://youtu.be/SFOMTchsTrs?t=221" target="_blank" class="image fit"><img src="images/small_windows_04-mini.png" alt="" /></a></div>
									<div class="col-6"><a href="https://youtu.be/cLv97Wqvh3k?list=PLAGUynfv3f-6xtlM1ovXPstNLZSRPlCp7&t=224" target="_blank" class="image fit"><img src="images/04_mop.png" alt="" /></a></div>
								</div>
							</div>
						</section>
					</div>
			</div>
			<div class="col-8 col-12-medium imp-medium">
				<!-- Content -->
				<div id="content">
					<section class="last">
						<h2>What is it ?</h2>
						<p>
						The Shadow is an online demoparty.<br>
						It is focused on presenting productions relative to the <a href="https://en.wikipedia.org/wiki/Demoscene" target="_blank">Demoscene culture</a>,<br>
						including so-called "demo": real-time non interactive audio-visual demonstration<br>
						on a variety of computer platforms.
						</p>

						<h2>Attending</h2>
						<h3>When ?</h3>
						The Shadow will take place online from <b>10th - 12th June 2022</b>
						<br/><br/>
						<h3>Where</h3>
						<p>
						Due to the uncertainty still prevalent with regards to the pandemic, the party will be held online. It will be streamed on Twitch, and text, voice and video chats will be available on <a href="https://discord.gg/2mEc4pXvWb" target="_blank">Discord</a>. However we keep looking for a location that will be confirmed soon. Follow up on <a href="https://www.shadow-party.org/infoline">https://www.shadow-party.org/infoline</a><br>
						You don’t need an account to watch the stream on Twitch. However if you want to join the <a href="https://discord.gg/2mEc4pXvWb" target="_blank">Discord</a> server you will need a <a href="https://discord.gg/2mEc4pXvWb" target="_blank">Discord</a> account.<br>
						</p>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>