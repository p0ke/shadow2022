<?php

?>
<div id="main-wrapper">
	<div class="container">
		<div id="content">
			<center><h1>Clic the compo you are looking for to unfold the rules</h1></center><br/>
			<!-- Content -->
			<div class="wrap-collabsible">
			  <input id="collapsible1" class="toggle" type="checkbox">
			  <label for="collapsible1" tabindex="0" class="lbl-toggle">Oldschool demo</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
								<li>Any console prior to 2005 and computer prior to 1995 are considered oldschool</li>
								<li>The demo must work on the real hardware</li>
								<li>Any additional real hardware can be used as long as it can be emulated (for example, 512k extension on AMIGA 500)</li>
								<li>Maximum duration: 8 minutes</li>
								<li>Maximum size: 2 media of the original machine (for example: 2 floppy disks on AMIGA, 2 faces of one disk on CPC)</li>
								<li>Please provide instructions on how to run your demo on the compo machine (or emulator)</li>
								<li>Please provide a video capture (720p or 1080p) of the demo</li>
							</ul>
					</div>
				</div>
			</div>
			<div class="wrap-collabsible">
			  <input id="collapsible2" class="toggle" type="checkbox">
			  <label for="collapsible2" tabindex="0" class="lbl-toggle">Oldschool intro</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
								<li>Any console prior to 2005 and computer prior to 1995 are considered oldschool</li>
								<li>The intro must work on the real hardware</li>
								<li>Any additional real hardware can be used as long as it can be emulated (for example, 512k extension on AMIGA 500)</li>
								<li>Maximum file size: 64kiB</li>
								<li>Maximum duration: 8 minutes</li>
								<li>Please provide instructions on how to run your demo on the compo machine (or emulator)</li>
								<li>Please provide a video capture (720p or 1080p) of the intro</li>
								<li>This compo will be merged with the “oldschool demo” compo if there are not enough entries</li>
							</ul>
					</div>
				</div>
			</div>

			<div class="wrap-collabsible">
			  <input id="collapsible3" class="toggle" type="checkbox">
			  <label for="collapsible3" tabindex="0" class="lbl-toggle">Newschool intro</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
								<li>Maximum duration: 8 minutes</li>
								<li>Maximum file size: 64kiB</li>
								<li>Please provide a video capture (720p or 1080p) of the intro. </li>
								<li>This compo will be merged with the “newschool demo” compo if there are not enough entries</li>
							</ul>
					</div>
				</div>
			</div>


			<div class="wrap-collabsible">
			  <input id="collapsible4" class="toggle" type="checkbox">
			  <label for="collapsible4" tabindex="0" class="lbl-toggle">Newschool Demo</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
								<li>Maximum duration: 8 minutes</li>
								<li>Please provide a video capture (720p or 1080p) of the demo</li>
							</ul>
					</div>
				</div>
			</div>

			<div class="wrap-collabsible">
			  <input id="collapsible5" class="toggle" type="checkbox">
			  <label for="collapsible5" tabindex="0" class="lbl-toggle">Oldschool Graphics</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
								<li>Accepted file format: PNG or executable binary</li>
								<li>Maximum resolution and color depth: 320x256 /32 colors</li>
								<li>Include at least 3 work in progress steps, together with the final version</li>
							</ul>
					</div>
				</div>
			</div>



			<div class="wrap-collabsible">
			  <input id="collapsible6" class="toggle" type="checkbox">
			  <label for="collapsible6" tabindex="0" class="lbl-toggle">Newschool Graphics</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
								<li>Render, scan or photo of hand painted or procedural material only</li>
								<li>Accepted file formats: PNG, JPEG, or executable binary</li>
								<li>Include at least 3 work in progress steps, together with the final version</li>
								<li>Quality settings: Baseline Standard instead of Optimized or Progressive, and sRGB color profile</li>
								<li>Maximum file size: 20MiB</li>
							</ul>
					</div>
				</div>
			</div>	
			<div class="wrap-collabsible">
			  <input id="collapsible13" class="toggle" type="checkbox">
			  <label for="collapsible13" tabindex="0" class="lbl-toggle">Textmode Graphics <img src ="images/new.gif" width="32" height="16"/></label>
			  <div class="collapsible-content">
			    <div class="content-inner">
						<ul>
							<li> Text based artwork</li>
							<li> Any system (ASCII/ANSI/PETSCII/SHARPSCII/..)</li>
							<li> You must use any "build-in" standard charset</li>
							<li><b>Custom charsets not allowed</b></li>
							<li> Mandatory informations: target hardware, display software, charset, font</li>
						</ul>
					</div>
				</div>
			</div>	
			<div class="wrap-collabsible">
			  <input id="collapsible7" class="toggle" type="checkbox">
			  <label for="collapsible7" tabindex="0" class="lbl-toggle">Music Streamed</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
				                <li>MP3/OGG</li>
				                <li>Required format: OGG or MP3</li>
				                <li>Maximum playing time: 3:30, longer songs will be faded out start 3:30</li>
				                <li>Playback with current Winamp 5.x version</li>
							</ul>
					</div>
				</div>
			</div>	


			<div class="wrap-collabsible">
			  <input id="collapsible8" class="toggle" type="checkbox">
			  <label for="collapsible8" tabindex="0" class="lbl-toggle">Music Tracked</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
				                <li>Required format: (XM / MOD or Arkostracker)</li>
				                <li>Maximum playing time: 3:30, longer songs will be faded out start 3:30</li>
				                <li>Must be 100% Fasttracker-compatible or Arkostracker, the compo will play the music inside the tracker</li>		
				                <li>Exotic format others than xm/mod are allowed if you can give a player/tracker that can display the track's content during replay</li>
							</ul>
					</div>
				</div>
			</div>	

			<div class="wrap-collabsible">
			  <input id="collapsible9" class="toggle" type="checkbox">
			  <label for="collapsible9" tabindex="0" class="lbl-toggle">Photos</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
							<ul>
								<li>Allowed retouching is limited to filtering, coloring grading, cropping, stitching, etc</li>
								<li>Collages or photomontages are not accepted and can be submitted to the Wild compo instead</li>
								<li>Include the original photo if the entry is retouched</li>
								<li>Accepted file formats: PNG or JPEG</li>
								<li>Quality settings: Baseline Standard instead of Optimized or Progressive, and sRGB color profile</li>
								<li> Maximum file size: 20MiB. </li>
							</ul>
					</div>
				</div>
			</div>	

			<div class="wrap-collabsible">
			  <input id="collapsible10" class="toggle" type="checkbox">
			  <label for="collapsible10" tabindex="0" class="lbl-toggle">Wild</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
						<ul>
							<li>Platform: any</li>
							<li>Maximum duration: 8mn</li>
							<li>Please provide a video (720p or 1080p) of the production</li>	
						</ul>
					</div>
				</div>
			</div>	
			<div class="wrap-collabsible">
			  <input id="collapsible11" class="toggle" type="checkbox">
			  <label for="collapsible11" tabindex="0" class="lbl-toggle">Fantasy Console</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
						<ul>
							<li>Platform: any fantasy console</li>
							<li>Maximum size: 256 bytes</li>
							<li>Please provide a video (720p or 1080p) of the production</li>	
						</ul>
					</div>
				</div>
			</div>	
			<div class="wrap-collabsible">
			  <input id="collapsible12" class="toggle" type="checkbox">
			  <label for="collapsible12" tabindex="0" class="lbl-toggle">Motion Design</label>
			  <div class="collapsible-content">
			    <div class="content-inner">
						<ul>
						<li>Any software</li>	
						<li>Maximum duration: 1:30</li>	
						<li>Maximum file size: 1 Go</li>	
						<li>Please provide a video capture (Minimum size : 1280x720)</li>	
						<li>Sound : no copyright content allowed</li>
						<li>Imposed Image : </li>	
							<br/><img src ="images/Motion-Slide_shadow-party.org.png" width="200" height="140"/>
						</ul>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>		